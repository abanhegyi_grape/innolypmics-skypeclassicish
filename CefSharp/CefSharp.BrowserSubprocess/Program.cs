﻿// Copyright © 2010-2017 The CefSharp Authors. All rights reserved.
//
// Use of this source code is governed by a BSD-style license that can be found in the LICENSE file.

using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;
using CefSharp.Internals;

namespace CefSharp.BrowserSubprocess
{
    public class Program
    {
        public static int Main(string[] args)
        {
#if DEBUG
            Debug.Listeners.Add(new TextWriterTraceListener($"debug_{Guid.NewGuid()}_.txt"));
#endif
            Debug.AutoFlush = true;
            Debug.WriteLine("BrowserSubprocess starting up with command line: " + String.Join("\n", args));


            var mutex = new Mutex(true, SkypeClassicish.Properties.Settings.Default.MutexName);

            new Task(async () =>
            {
                while (true)
                {
                    if (mutex.WaitOne(0))   //if the program managed to get the mutex, SkypeClassicish doesn't run anymore -> close
                    {
                        mutex.ReleaseMutex();
                        Environment.Exit(0);
                    }
                    else
                    {
                        await Task.Delay(500);
                    }
                }
            }, TaskCreationOptions.LongRunning).Start();

            SubProcess.EnableHighDPISupport();

            int result;
            var type = args.GetArgumentValue(CefSharpArguments.SubProcessTypeArgument);

            var parentProcessId = -1;

            // The Crashpad Handler doesn't have any HostProcessIdArgument, so we must not try to
            // parse it lest we want an ArgumentNullException.
            if (type != "crashpad-handler")
            {
                parentProcessId = int.Parse(args.GetArgumentValue(CefSharpArguments.HostProcessIdArgument));
                // true, because fck it
                if (true || args.HasArgument(CefSharpArguments.ExitIfParentProcessClosed))
                {
                    Debug.WriteLine("ExitIfParentProcessClosed set!");
                    Task.Factory.StartNew(() => AwaitParentProcessExit(parentProcessId), TaskCreationOptions.LongRunning);
                }
            }

            // Use our custom subProcess provides features like EvaluateJavascript
            if (type == "renderer")
            {
                var wcfEnabled = args.HasArgument(CefSharpArguments.WcfEnabledArgument);
                var subProcess = wcfEnabled ? new WcfEnabledSubProcess(parentProcessId, args) : new SubProcess(args);

                using (subProcess)
                {
                    result = subProcess.Run();
                }
            }
            else
            {
                result = SubProcess.ExecuteProcess();
            }

            Debug.WriteLine("BrowserSubprocess shutting down.");
            
            return result;
        }

        private static async void AwaitParentProcessExit(int parentProcessId) 
        {
            try 
            {
                var parentProcess = Process.GetProcessById(parentProcessId);
                parentProcess.WaitForExit();
            }
            catch (Exception e) 
            {
                //main process probably died already
                Debug.WriteLine(e);
            }

            await Task.Delay(1000); //wait a bit before exiting

            Debug.WriteLine("BrowserSubprocess shutting down forcibly.");

            Environment.Exit(0);
        }
    }
}
