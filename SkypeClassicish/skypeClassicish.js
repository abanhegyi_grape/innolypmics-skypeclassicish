document.addEventListener("paste",
    function (event)
    {
        var hasImage = JavaScriptClipboardFileHandler.hasClipboardImage();
        var hasFiles = JavaScriptClipboardFileHandler.hasClipboardFiles();

        if (hasImage || hasFiles)
        {
            //don't let skype handle it
            event.stopPropagation();
            event.preventDefault();

            var dataTransferObject = new DataTransfer();

            Object.defineProperty(dataTransferObject, "files", { value: new Array(), writeable: true }); //overwrite protected 'files' so that we can actually add files
            Object.defineProperty(dataTransferObject, "types", { value: new Array(), writeable: true }); //overwrite protected 'types' so that we can tell skype that we have files
            dataTransferObject.types.push("Files");

            if (hasImage)
            {
                AddImage(dataTransferObject);
            }
            else
            {
                AddFiles(dataTransferObject);
            }

            var dropEv = new DragEvent("drop",
                {
                    "dataTransfer": dataTransferObject
                });

            //the div that handles drag events AND is the current handler
            document.querySelector(":not(.hide) > .conversationControl.chat.themeLight.allowTextSelection").dispatchEvent(dropEv);
        }
    },
    true /*capture phase instead of bubble phase*/);

function AddFiles(dataTransferObject)
{
    var fileCount = JavaScriptClipboardFileHandler.getFileCount();

    for (var i = 0; i < fileCount; i++)
    {
        var data = JavaScriptClipboardFileHandler.getFileDataBase64(i);
        var mime = JavaScriptClipboardFileHandler.getFileMimeType(i);
        var filename = JavaScriptClipboardFileHandler.getFileName(i);

        var file = Base64ToFile(data, filename, mime);

        dataTransferObject.files.push(file);
    }

    return dataTransferObject;
}

function AddImage(dataTransferObject)
{
    var data = JavaScriptClipboardFileHandler.getImageDataBase64();
    var mime = JavaScriptClipboardFileHandler.getImageMimeType();
    var imagename = JavaScriptClipboardFileHandler.getRandomImageName();

    var image = Base64ToFile(data, imagename, mime);

    dataTransferObject.files.push(image);

    return dataTransferObject;
}

function Base64ToFile(base64, filename, mime)
{
    var byteString = atob(base64);

    var arrBuff = new ArrayBuffer(byteString.length);
    var intArr = new Uint8Array(arrBuff);
    for (var i = 0; i < byteString.length; i++)
    {
        intArr[i] = byteString.charCodeAt(i);
    }

    var file = new File([arrBuff],
        filename,
        {
            type: mime
        });

    return file;
}

async function DownloadFromUrl(url)
{
    HighlightImage(url);
    AddBorderToImage(url, "blue");
    //got headers from chrome, surely not even half of them are needed, but I won't touch it :)
    var headers = new Headers(
        {
            "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8",
            "Accept-Encoding": "gzip, deflate, br",
            "Accept-Language": "en-US,en;q=0.9",
            "Cache-Control": "max-age=0",
            "Connection": "keep-alive",
            "Cookie": document.cookie
        });
    try
    {
        //fetch the file from the url
        //also adding marker to know that it was requested from here
        var response = await fetch(url,
            { "method": "GET", credentials: "include", headers: headers });

        if (!response.ok)
        {
            AddBorderToImage(url, "red");
            return;
        }

        var blob = await response.blob();

        var reader = new FileReader();

        //because it has to be an event...
        reader.onloadend = function ()
        {
            //blob to binary string to base64 for dataurl
            var base64 = btoa(reader.result);

            var mimeType = response.headers.get("content-type");

            //get filename
            var splitUrl = url.split("/");
            var filename = splitUrl[splitUrl.indexOf("views") - 1] + window.JavaScriptMimeTypeHelper.getExtensionForMimeType(mimeType);

            //set download link up
            var link = document.createElement("a");
            link.style.display = "none";
            link.download = filename;
            link.href = `data:${mimeType};charset=utf-8;base64,${base64}`;

            //download!
            link.click();

            //don't litter the DOM
            link.remove();
            AddBorderToImage(url, "green");
        };

        reader.onerror = function ()
        {
            AddBorderToImage(url, "red");
        }
        reader.onabort = reader.onerror;

        reader.readAsBinaryString(blob);
    }
    catch (err)
    {
        AddBorderToImage(url, "red");
    }
}

function SetFooter(show)
{
    var footer = document.getElementById("footer");

    if (!footer)
    {
        if (!footerObserver)
        {
            footerObserver = new MutationObserver(function (mutationList, callBack)
            {
                footer = document.getElementById("footer");

                if (footer)
                {
                    footerObserver.disconnect();
                    footerObserver = null; //GC cleanup please
                    SetFooter(show);
                }
            });
            footerObserver.observe(document.body, { childList: true });
        }
    }
    else
    {
        if (show)
        {
            footer.style.display = "";      //resets state defined by css
        }
        else
        {
            footer.style.display = "none";  //hides it
        }
    }
}

function RemoveByQuery(query)
{
    window.SkypeClassicish.RemoveQueries.push(query);

    var element = document.querySelector(query);

    if (element)
    {
        element.remove();
    }
}

window.SkypeClassicish = {
    Observer: "",
    RemoveQueries: new Array()
};

window.SkypeClassicish.Observer = new MutationObserver(function ()
{
    document.dispatchEvent(new CustomEvent("SkypeClassicishMutation"));
});

document.addEventListener("SkypeClassicishMutation", function()
    {
        SkypeClassicish.RemoveQueries.forEach(function(query)
        {
            var element = document.querySelector(query);

            if (element)
            {
                element.remove();
            }
        });
    });

window.SkypeClassicish.Observer.observe(document, { childList: true, subtree: true });  //document.body didn't work for some reason...

function RemoveUselessStuff()
{
    RemoveByQuery("#msccBanner");
    RemoveByQuery("#notificationsBanner");
    RemoveByQuery("#notifications");
    RemoveByQuery(".notifications.shell");
    RemoveWelcomeOverlay();
}

function RemoveWelcomeOverlay()
{
    var query = ".overlayContainer.swx.desktop";
    document.addEventListener("SkypeClassicishMutation", function()
    {
        var element = document.querySelector(query);    //if multiple, the remove will cause another event mutation event to fire, so we can go 1 by 1

        if (element)
        {
            if (element.querySelector("div[aria-label~=Welcome]"))  //it's the welcome overlay, useless...
            {
                element.remove();
            }

        }
    });
}

document.addEventListener("click", function (event)
{
    //we need this because CEF only gave me the target for browser popups (anything opening in new tab/window) and couldn't get the HREF another way...
    var anchor = GetAncestor('a', event.target);
    if (anchor)
    {
        if (anchor.href.indexOf("api.asm.skype.com/v1/objects") !== -1) //don't mess up download links
        {
            return;
        }

        if (anchor.rel) //rel is "" if it's for internal navigation, else "noopener noreferrer"
        {
            window.open(anchor.href, "_self");
            event.preventDefault();
            event.stopPropagation();
        }
    }
}, true);

function GetAncestor(tagname, element)
{
    if (!element)
    {
        return false;
    }

    if (element.tagName.toUpperCase() === tagname.toUpperCase())
    {
        return element;
    }

    return GetAncestor(tagname, element.parentElement);
}


function HighlightImage(url)
{
    var image = document.querySelector(`a.thumbnailHolder[href="${url}"]`);
    Highlight(image);
}

function HighlightFile(filename)
{
    //we get all as we quite literally can't tell which one is the right one here...
    var fileElements = document.querySelectorAll(`p.FileTransfer a[title~="${filename}"]`);
    for (var i = 0; i < fileElements.length; i++)
    {
        Highlight(fileElements[i].parentElement.parentElement);   //no need to errorcheck, must have at least 2 parents
    }
}

function Highlight(element)
{
    try
    {
        var keyframes = [{ filter: "brightness(1)" }, { filter: "brightness(0.2)" }];

        var animation = element.animate(keyframes, { duration: 10, fill: "both" });
        animation.onfinish = function ()
        {
            element.animate(keyframes, { duration: 1500, fill: "both", direction: "reverse" });
        };
    }
    catch (err)
    {/*just so this doesn't throw errors*/ }
}

function AddBorderToImage(url, borderColor)
{
    var image = document.querySelector(`a.thumbnailHolder[href="${url}"]`);
    AddBorder(image, borderColor);
}

function AddBorderToFile(filename, borderColor)
{
    //we get all as we quite literally can't tell which one is the right one here...
    var fileElements = document.querySelectorAll(`p.FileTransfer a[title~="${filename}"]`);
    for (var i = 0; i < fileElements.length; i++)
    {
        AddBorder(fileElements[i].parentElement.parentElement, borderColor); //no need to errorcheck, must have at least 2 parents
    }
}

function AddBorder(element, borderColor)
{
    try
    {
        element.style.borderColor = borderColor;
        element.style.borderWidth = "medium";
        element.style.borderStyle = "solid";
    }
    catch (err) 
    {/*just so this doesn't throw errors if element is null or something*/ }
}

RemoveUselessStuff();