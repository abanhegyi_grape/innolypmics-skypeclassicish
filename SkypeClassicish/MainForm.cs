using System;
using System.Diagnostics;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;
using CefSharp;
using CefSharp.WinForms;
using SkypeClassicish.BoundJsObjects;
using SkypeClassicish.CustomEventArgs;
using SkypeClassicish.Handlers;
using SkypeClassicish.Helpers;

namespace SkypeClassicish
{
    public partial class MainForm : Form
    {
        public ChromiumWebBrowser ChromiumSkype { get; set; }
        private FormWindowState LastState { get; set; }

        private int notificationNumber;
        private int NotificationNumber
        {
            get => notificationNumber;
            set
            {
                notificationNumber = value;
                SetIcon();
            }
        }

        public MainForm()
        {
            Debug.WriteLine(Process.GetCurrentProcess().Id);

            InitializeComponent();
            InitializeChromium();

            notifyIcon1.ContextMenuStrip = notifyIconContextMenu;
            notifyIcon1.DoubleClick += notifyIcon1_DoubleClick;
            notifyIcon1.Visible = true;

            Properties.Settings.Default.SettingsSaving += (o, e) => { SetFooter(Properties.Settings.Default.ShowFooter); };
        }

        private Icon DrawNotificationOnIcon(Icon icon, int percentage)
        {
            const float PEN_WIDTH_PERCENTAGE = 0.02f;   //seems like ok

            var bitmap = icon.ToBitmap();
            var graphics = Graphics.FromImage(bitmap);

            var size = bitmap.Width;    //assume we have a square bounded icon
            var notifDiam = size * percentage / 100f;
            var position = size - notifDiam - 1;  //-1 because zero based index
            var penWidth = notifDiam * PEN_WIDTH_PERCENTAGE;

            var innerRectanglePos = size - notifDiam / (float)Math.Sqrt(2);
            var circleCenter = position + notifDiam/2;
            
            graphics.FillEllipse(Brushes.Orange, position, position, notifDiam, notifDiam);
            graphics.DrawEllipse(new Pen(Brushes.White, penWidth), position, position, notifDiam, notifDiam);


            FontFamily fontFamily = new FontFamily("Arial");
            Font font = new Font(fontFamily, notifDiam, FontStyle.Bold, GraphicsUnit.Pixel);

            var format = new StringFormat
            {
                LineAlignment = StringAlignment.Center,
                Alignment = StringAlignment.Center
            };
            graphics.DrawString(NotificationNumber.ToString(), font, Brushes.White, circleCenter, circleCenter + notifDiam*0.1f, format);

            return Icon.FromHandle((bitmap.Clone() as Bitmap).GetHicon());
        }

        private void SetIcon()
        {
            Icon formIcon;
            Icon sysTrayIcon;

            if (NotificationNumber > 0)
            {
                formIcon = DrawNotificationOnIcon(Properties.Resources.Icon, 60);
                sysTrayIcon = DrawNotificationOnIcon(Properties.Resources.Icon, 80);
            }
            else
            {
                formIcon = Properties.Resources.Icon;
                sysTrayIcon = Properties.Resources.Icon;
            }

            MethodInvoker del = delegate
            {
                this.Icon = formIcon;
                notifyIcon1.Icon = sysTrayIcon;
                this.Refresh();
            };

            if (InvokeRequired)
            {
                this.Invoke(del);
            }
            else
            {
                del.Invoke();
            }
        }

        private void SetFooter(bool show)
        {
            if (ChromiumSkype.CanExecuteJavascriptInMainFrame)
            {
                ChromiumSkype.ExecuteScriptAsync("SetFooter", show);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            LoadSettings();
        }

        private void LoadSettings()
        {
            var loc = new Point(Properties.Settings.Default.LocationX, Properties.Settings.Default.LocationY);
            if (!loc.IsEmpty)
            {
                this.Location = loc;
            }

            LastState = (FormWindowState)Properties.Settings.Default.WindowsState;

            if (Properties.Settings.Default.StartMinimized)
            {
                this.WindowState = FormWindowState.Minimized;
            }
            else
            {
                this.WindowState = LastState;
            }

            var size = new Size(Properties.Settings.Default.Width, Properties.Settings.Default.Height);
            if (!size.IsEmpty)
            {
                this.Size = size;
            }

            (ChromiumSkype.RenderProcessMessageHandler as CustomRenderProcessMessageHandler).OnContextCreatedEvent += async () =>
            {
                await Task.Delay(50);   //minor delay so that we can actually load in skypeClassicish.js BEFORE this is called
                SetFooter(Properties.Settings.Default.ShowFooter);
            };
        }

        private void SaveSettings(bool saveToFile = false)  //saveToFile -> avoid writing to disk overhead when just saving the new state in general
        {
            Properties.Settings.Default.LocationX = Location.X;
            Properties.Settings.Default.LocationY = Location.Y;

            Properties.Settings.Default.WindowsState = (int)(this.WindowState == FormWindowState.Minimized ? FormWindowState.Normal : this.WindowState);

            if (this.WindowState == FormWindowState.Normal)
            {
                Properties.Settings.Default.Width = Size.Width;
                Properties.Settings.Default.Height = Size.Height;
            }

            if (saveToFile)
            {
                Properties.Settings.Default.Save();
            }
        }

        public void InitializeChromium()
        {
            Cef.Initialize(GetCefSettings());

            CefSharpSettings.SubprocessExitIfParentProcessClosed = true;
            CefSharpSettings.LegacyJavascriptBindingEnabled = true;         //no need, won't touch

            ChromiumSkype = new ChromiumWebBrowser("web.skype.com")
            {
                Dock = DockStyle.Fill,
                RequestHandler = new CustomRequestHandler(),
                FocusHandler = new CustomFocusHandler(),
                LifeSpanHandler = new CustomLifeSpanHandler(),
                RenderProcessMessageHandler = new CustomRenderProcessMessageHandler(),
                DownloadHandler = new CustomDownloadHandler()
            };

            Controls.Add(ChromiumSkype);

            BindJsObjects();
            IncludeJavaScriptLibrary("skypeClassicish.js");

            ChromiumSkype.TitleChanged += TitleChangedEvent;
            ChromiumSkype.LoadError += LoadErrorEvent;
            ChromiumSkype.FrameLoadStart += FrameLoadStart;

            ChromiumSkype.BrowserSettings.BackgroundColor = 0xFF00ADFFu;
            (ChromiumSkype.DownloadHandler as CustomDownloadHandler).OnDownloadUpdatedEvent += OnDownloadUpdated;

        }

        private void OnDownloadUpdated(object sender, DownloadItem downloadItem)
        {
            if (downloadItem.IsComplete)
            {
                ShowDownloadNotification(downloadItem);
            }
        }

        private bool alreadyReloading = false;
        private bool reloadNeeded = false;
        private void LoadErrorEvent(object sender, LoadErrorEventArgs args)
        {
            const int DELAY_TIMER = 10000;  //10 seconds

            if (args.Frame != ChromiumSkype.GetMainFrame() || alreadyReloading)
            {
                return;
            }

            if (!reloadNeeded)
            {
                Task.Run(() => { MessageBox.Show($"Can't load the page. Trying again every 10 seconds from now on until success. Sorry for the inconvenience.\nThe error was:{args.ErrorText}", $"Page load error {args.ErrorCode}"); });
            }

            alreadyReloading = true;

            new Task(() => 
            {
                Task.Delay(DELAY_TIMER);

                alreadyReloading = false;

                if (reloadNeeded)
                {
                    ChromiumSkype.Reload();
                }
                
            }, TaskCreationOptions.LongRunning).Start();

        }

        private void FrameLoadStart(object sender, FrameLoadStartEventArgs args)
        {
            if (ChromiumSkype.IsBrowserInitialized && args.Frame == ChromiumSkype.GetMainFrame())
            {
                reloadNeeded = false;
            }
        }


        private void TitleChangedEvent(object sender, TitleChangedEventArgs args)
        {
            var regexResult = Regex.Match(args.Title, @"(?<=\()[0-9]+(?=\))").Value;

            if (regexResult != string.Empty && int.TryParse(regexResult, out int notifNumber))
            {
                NotificationNumber = notifNumber;
            }
            else
            {
                NotificationNumber = 0;
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs args)
        {
            if (Properties.Settings.Default.CloseMinimizes && args.CloseReason == CloseReason.UserClosing)
            {
                args.Cancel = true;
                this.WindowState = FormWindowState.Minimized;
                return;
            }

            SaveSettings(true);

            this.Hide();

            Task.Run(async () =>
            {
                await Task.Delay(10000);
                Debug.WriteLine("ForceClosed!");
                Environment.Exit(0);
            }); //if the app hangs while closing because of CefMagicery, kill it (within 10 seconds)

            try
            {
                if (ChromiumSkype != null)
                {
                    if (Controls.Contains(ChromiumSkype))
                    {
                        this.Controls.Remove(ChromiumSkype);
                    }


                    if (!ChromiumSkype.Disposing && !ChromiumSkype.IsDisposed)
                    {
                        ChromiumSkype.Dispose();
                    }
                }

                if (Cef.IsInitialized)
                {
                    Cef.Shutdown();
                }
            }
            catch (Exception e)
            {
                Debug.WriteLine(e.ToString());
            }
        }

        private void notifyIcon1_DoubleClick(object sender, EventArgs e)
        {
            this.Restore();
        }

        protected override void OnResize(EventArgs e)
        {
            SaveSettings();

            //what is this for? 
            bool cursorNotInBar = Screen.GetWorkingArea(this).Contains(Cursor.Position);

            if (this.WindowState != FormWindowState.Minimized)
            {
                LastState = this.WindowState;

                if (ChromiumSkype != null && !this.Controls.Contains(ChromiumSkype))
                {
                    this.Restore();
                }
            }

            if (this.WindowState == FormWindowState.Minimized && cursorNotInBar)
            {
                HandleMinimize();
            }

            base.OnResize(e);
        }

        private CefSettings GetCefSettings()
        {
            CefSettings settings = new CefSettings();
            settings.CefCommandLineArgs.Add("enable-media-stream", "1");
            settings.CefCommandLineArgs.Add("allow-hidden-media-playback", "1");
            settings.CefCommandLineArgs.Add("allow-http-screen-capture", "1");
            settings.CefCommandLineArgs.Add("enable-web-notification-custom-layouts", "1");
            settings.CefCommandLineArgs.Add("enabled-new-style-notification", "1");
            settings.CefCommandLineArgs.Add("enable-push-api-background-mode", "1");
            settings.CachePath = Properties.Settings.Default.CachePath == string.Empty ? System.IO.Path.GetDirectoryName(Application.ExecutablePath) : Properties.Settings.Default.CachePath;
            settings.PersistUserPreferences = true;

            return settings;
        }

        private void BindJsObjects()
        {
            ChromiumSkype.JavascriptObjectRepository.Register("JavaScriptNotificationHandler", new JavaScriptNotificationHandler(), true);
            ChromiumSkype.JavascriptObjectRepository.Register("JavaScriptClipboardFileHandler", new JavaScriptClipboardFileHandler(), false);
            ChromiumSkype.JavascriptObjectRepository.Register("JavaScriptMimeTypeHelper", new JavaScriptMimeTypeHelper(), false);

            (ChromiumSkype.RenderProcessMessageHandler as CustomRenderProcessMessageHandler).OnContextCreatedEvent += () =>
            {
                ChromiumSkype.ExecuteScriptAsync("CefSharp.BindObjectAsync(\"JavaScriptNotificationHandler\", \"JavaScriptNotificationHandler\");");
                ChromiumSkype.ExecuteScriptAsync("CefSharp.BindObjectAsync(\"JavaScriptClipboardFileHandler\", \"JavaScriptClipboardFileHandler\");");
                ChromiumSkype.ExecuteScriptAsync("CefSharp.BindObjectAsync(\"JavaScriptMimeTypeHelper\", \"JavaScriptMimeTypeHelper\");");
            };

            JavaScriptNotificationHandler.OnJsNotification += ShowNotification;
        }

        private void IncludeJavaScriptLibrary(string path)
        {
            (ChromiumSkype.RenderProcessMessageHandler as CustomRenderProcessMessageHandler).OnContextCreatedEvent += async () =>
            {
                using (var reader = new System.IO.StreamReader(path))
                {
                    ChromiumSkype.ExecuteScriptAsync(await reader.ReadToEndAsync());
                }
            };
        }

        private void ShowDownloadNotification(DownloadItem downloadItem)
        {
            void OnClick(object o, EventArgs e)
            {
                BrowserHelpers.TryOpen(downloadItem.FullPath);
                notifyIcon1.BalloonTipClicked -= OnClick;
            }

            notifyIcon1.BalloonTipClicked += OnClick;

            var filename = System.IO.Path.GetFileName(downloadItem.FullPath);

            notifyIcon1.BalloonTipTitle = "Download finished";
            notifyIcon1.BalloonTipText = $"The file {filename} has finished downloading!";

            notifyIcon1.ShowBalloonTip(1 /*deprecated, value doesn't matter as of Windows Vista*/);
        }

        private void ShowNotification(JsNotificationEventArgs args)
        {
            async void OnClick(object o, EventArgs e)
            {
                await ExecuteJsCallback(args.OnClick);

                this.Restore();

                notifyIcon1.BalloonTipClicked -= OnClick;
            }

            async void OnClose(object o, EventArgs e)
            {
                await ExecuteJsCallback(args.OnClose);
                notifyIcon1.BalloonTipClosed -= OnClose;
            }

            async void OnShown(object o, EventArgs e)
            {
                await ExecuteJsCallback(args.OnShow);
                notifyIcon1.BalloonTipShown -= OnShown;
            }

            if (!Properties.Settings.Default.EnableNotifications)
            {
                return;
            }

            notifyIcon1.BalloonTipClicked += OnClick;
            notifyIcon1.BalloonTipClosed += OnClose;
            notifyIcon1.BalloonTipShown += OnShown;

            notifyIcon1.BalloonTipTitle = args.Title;
            notifyIcon1.BalloonTipText = args.Text;

            notifyIcon1.ShowBalloonTip(1 /*deprecated, value doesn't matter as of Windows Vista*/);
        }

        private async Task ExecuteJsCallback(IJavascriptCallback callback)
        {
            if (callback.CanExecute && !callback.IsDisposed)
            {
                await callback.ExecuteAsync();
            }
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SettingsForm s = new SettingsForm();

            // Define the border style of the form to a dialog box.
            s.FormBorderStyle = FormBorderStyle.FixedDialog;

            // Set the MaximizeBox to false to remove the maximize box.
            s.MaximizeBox = false;

            // Set the MinimizeBox to false to remove the minimize box.
            s.MinimizeBox = false;

            // Set the start position of the form to the center of the screen.
            s.StartPosition = FormStartPosition.CenterScreen;

            s.ShowDialog(this);
        }

        protected override void WndProc(ref Message m)
        {
            if (m.Msg == NativesHelper.FocusMessageBroadcastMessageId)
            {
                this.Restore();
                this.Activate();
            }

            base.WndProc(ref m);
        }

        private void Restore()
        {
            this.Visible = true;
            ChromiumSkype.Visible = true;
            Controls.Add(ChromiumSkype);
            ChromiumSkype.Refresh();
            this.WindowState = LastState;
            this.Show();
            this.Activate();
            this.TopMost = true;        //I'm literally stacking everythig so that they definitely work...
            this.TopMost = false;
            this.SetFocusToForm();
            this.Show();
            this.Activate();
        }

        private void HandleMinimize()
        {
            Controls.Remove(ChromiumSkype);

            if (Properties.Settings.Default.MinimizeToSystemTray)
            {
                this.Hide();
            }
        }
    }
}
