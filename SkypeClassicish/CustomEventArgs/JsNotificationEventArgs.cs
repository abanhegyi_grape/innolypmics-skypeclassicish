﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CefSharp;

namespace SkypeClassicish.CustomEventArgs
{
    public class JsNotificationEventArgs : System.EventArgs
    {
        public JsNotificationEventArgs(string title, string text, string iconUri, IJavascriptCallback onShow, IJavascriptCallback onClick, IJavascriptCallback onClose, IJavascriptCallback onError)
        {
            this.Title = title;
            this.Text = text;
            this.IconUri = iconUri;

            this.OnShow = onShow;
            this.OnClick = onClick;
            this.OnClose = onClose;
            this.OnError = onError;
        }


        public string Title { get; set; }
        public string Text { get; set; }
        public string IconUri { get; set; }

        public IJavascriptCallback OnShow { get; set; }
        public IJavascriptCallback OnClick { get; set; }
        public IJavascriptCallback OnClose { get; set; }
        public IJavascriptCallback OnError { get; set; }
    }
}
