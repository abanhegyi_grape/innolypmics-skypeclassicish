﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using SkypeClassicish.Helpers;

namespace SkypeClassicish.BoundJsObjects
{
    public class JavaScriptClipboardFileHandler
    {
        public System.Collections.Specialized.StringCollection FileList => ClipboardHelper.GetFileDropList();

        public bool HasClipboardFiles() => ClipboardHelper.ContainsFileDropList();

        public bool HasClipboardImage() => ClipboardHelper.ContainsImage();

        public int GetFileCount() => FileList.Count;

        public string GetFileName(int index)
        {
            if (index >= GetFileCount())
            {
                return string.Empty;
            }

            return Path.GetFileName(FileList[index]);
        }

        public string GetFileMimeType(int index)
        {
            if (index >= GetFileCount())
            {
                return string.Empty;
            }

            return Helpers.MimeTypes.MimeTypeMap.GetMimeType(Path.GetExtension(FileList[index]));
        }

        public string GetFileDataBase64(int index)
        {
            if (index >= GetFileCount() && File.Exists(FileList[index]))
            {
                return string.Empty;
            }

            try
            {
                //can fail on overly large files, but we won't handle that, just return string.Empty instead
                var fileData = File.ReadAllBytes(FileList[index]);
                return Convert.ToBase64String(fileData);
            }
#pragma warning disable
            catch (Exception e)
            {
                return string.Empty;
            }
#pragma warning restore
        }

        public string GetImageDataBase64()
        {
            if (!HasClipboardImage())
            {
                return string.Empty;
            }

            var image = new Bitmap(ClipboardHelper.GetImage());
            using (var imageData = new MemoryStream())
            {
                image.Save(imageData, ImageFormat.Jpeg);
                return Convert.ToBase64String(imageData.ToArray());
            }
        }

        public string GetImageMimeType() => "image/jpeg";

        public string GetRandomImageName() => Path.GetRandomFileName().Replace(".", "") + ".jpg";
    }
}
