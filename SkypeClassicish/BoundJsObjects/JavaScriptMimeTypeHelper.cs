﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SkypeClassicish.Helpers.MimeTypes;

namespace SkypeClassicish.BoundJsObjects
{
    public class JavaScriptMimeTypeHelper
    {
        public string GetExtensionForMimeType(string mimeType) => MimeTypeMap.GetExtension(mimeType);
    }
}
