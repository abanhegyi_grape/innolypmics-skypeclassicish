using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CefSharp;
using SkypeClassicish.CustomEventArgs;

namespace SkypeClassicish.BoundJsObjects
{
    public delegate void JsNotificationEventHandler(JsNotificationEventArgs args);

    public class JavaScriptNotificationHandler
    {
        public static event JsNotificationEventHandler OnJsNotification;

        public int FireNotification(string title, string text, string iconUri, IJavascriptCallback onClick, IJavascriptCallback onShow, IJavascriptCallback onError, IJavascriptCallback onClose)
        {
            var jsNotArgs = new JsNotificationEventArgs(title, text, iconUri, onShow, onClick, onClose, onError);
            OnJsNotification?.Invoke(jsNotArgs);
            return 0;
        }
    }
}
