﻿namespace SkypeClassicish
{
    partial class SettingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsForm));
            this.CachePathTB = new System.Windows.Forms.TextBox();
            this.CachePathL = new System.Windows.Forms.Label();
            this.DownloadPathL = new System.Windows.Forms.Label();
            this.DownloadPathTB = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.StartMinimizedCB = new System.Windows.Forms.CheckBox();
            this.RemoveFooterCB = new System.Windows.Forms.CheckBox();
            this.EnableNotiCB = new System.Windows.Forms.CheckBox();
            this.MinimizeToSysTrayCB = new System.Windows.Forms.CheckBox();
            this.CloseMinimizesCB = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.ResetSettingsButton = new System.Windows.Forms.Button();
            this.CancelButton = new System.Windows.Forms.Button();
            this.ApplyButton = new System.Windows.Forms.Button();
            this.OkButton = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // CachePathTB
            // 
            this.CachePathTB.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.CachePathTB.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.CachePathTB.Location = new System.Drawing.Point(91, 25);
            this.CachePathTB.Name = "CachePathTB";
            this.CachePathTB.ReadOnly = true;
            this.CachePathTB.Size = new System.Drawing.Size(313, 20);
            this.CachePathTB.TabIndex = 0;
            this.CachePathTB.TextChanged += new System.EventHandler(this.CachePathTB_TextChanged);
            this.CachePathTB.Enter += new System.EventHandler(this.CachePathTB_Enter);
            // 
            // CachePathL
            // 
            this.CachePathL.AutoSize = true;
            this.CachePathL.Location = new System.Drawing.Point(6, 29);
            this.CachePathL.Name = "CachePathL";
            this.CachePathL.Size = new System.Drawing.Size(62, 13);
            this.CachePathL.TabIndex = 1;
            this.CachePathL.Text = "Cache path";
            // 
            // DownloadPathL
            // 
            this.DownloadPathL.AutoSize = true;
            this.DownloadPathL.Location = new System.Drawing.Point(6, 22);
            this.DownloadPathL.Name = "DownloadPathL";
            this.DownloadPathL.Size = new System.Drawing.Size(79, 13);
            this.DownloadPathL.TabIndex = 3;
            this.DownloadPathL.Text = "Download path";
            // 
            // DownloadPathTB
            // 
            this.DownloadPathTB.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.DownloadPathTB.Cursor = System.Windows.Forms.Cursors.Default;
            this.DownloadPathTB.Location = new System.Drawing.Point(91, 19);
            this.DownloadPathTB.Name = "DownloadPathTB";
            this.DownloadPathTB.ReadOnly = true;
            this.DownloadPathTB.Size = new System.Drawing.Size(313, 20);
            this.DownloadPathTB.TabIndex = 2;
            this.DownloadPathTB.WordWrap = false;
            this.DownloadPathTB.TextChanged += new System.EventHandler(this.DownloadPathTB_TextChanged);
            this.DownloadPathTB.Enter += new System.EventHandler(this.DownloadPathTB_Enter);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.DownloadPathTB);
            this.groupBox1.Controls.Add(this.DownloadPathL);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(410, 52);
            this.groupBox1.TabIndex = 6;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "File options";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.StartMinimizedCB);
            this.groupBox2.Controls.Add(this.RemoveFooterCB);
            this.groupBox2.Controls.Add(this.EnableNotiCB);
            this.groupBox2.Controls.Add(this.MinimizeToSysTrayCB);
            this.groupBox2.Controls.Add(this.CloseMinimizesCB);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Location = new System.Drawing.Point(12, 75);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(410, 96);
            this.groupBox2.TabIndex = 7;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Display options";
            // 
            // StartMinimizedCB
            // 
            this.StartMinimizedCB.AutoSize = true;
            this.StartMinimizedCB.Location = new System.Drawing.Point(184, 48);
            this.StartMinimizedCB.Name = "StartMinimizedCB";
            this.StartMinimizedCB.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.StartMinimizedCB.Size = new System.Drawing.Size(96, 17);
            this.StartMinimizedCB.TabIndex = 9;
            this.StartMinimizedCB.Text = "Start minimized";
            this.StartMinimizedCB.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.StartMinimizedCB.UseMnemonic = false;
            this.StartMinimizedCB.UseVisualStyleBackColor = true;
            this.StartMinimizedCB.CheckedChanged += new System.EventHandler(this.StartMinimizedCB_CheckedChanged);
            // 
            // RemoveFooterCB
            // 
            this.RemoveFooterCB.AutoSize = true;
            this.RemoveFooterCB.Location = new System.Drawing.Point(184, 25);
            this.RemoveFooterCB.Name = "RemoveFooterCB";
            this.RemoveFooterCB.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.RemoveFooterCB.Size = new System.Drawing.Size(96, 17);
            this.RemoveFooterCB.TabIndex = 7;
            this.RemoveFooterCB.Text = "Remove footer";
            this.RemoveFooterCB.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.RemoveFooterCB.UseMnemonic = false;
            this.RemoveFooterCB.UseVisualStyleBackColor = true;
            this.RemoveFooterCB.CheckedChanged += new System.EventHandler(this.RemoveFooterCB_CheckedChanged);
            // 
            // EnableNotiCB
            // 
            this.EnableNotiCB.AutoSize = true;
            this.EnableNotiCB.Location = new System.Drawing.Point(26, 71);
            this.EnableNotiCB.Name = "EnableNotiCB";
            this.EnableNotiCB.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.EnableNotiCB.Size = new System.Drawing.Size(118, 17);
            this.EnableNotiCB.TabIndex = 8;
            this.EnableNotiCB.Text = "Enable notifications";
            this.EnableNotiCB.UseVisualStyleBackColor = true;
            this.EnableNotiCB.CheckedChanged += new System.EventHandler(this.EnableNotiCB_CheckedChanged);
            // 
            // MinimizeToSysTrayCB
            // 
            this.MinimizeToSysTrayCB.AutoSize = true;
            this.MinimizeToSysTrayCB.Location = new System.Drawing.Point(5, 48);
            this.MinimizeToSysTrayCB.Name = "MinimizeToSysTrayCB";
            this.MinimizeToSysTrayCB.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.MinimizeToSysTrayCB.Size = new System.Drawing.Size(139, 17);
            this.MinimizeToSysTrayCB.TabIndex = 7;
            this.MinimizeToSysTrayCB.Text = "Minimize to System Tray";
            this.MinimizeToSysTrayCB.UseVisualStyleBackColor = true;
            this.MinimizeToSysTrayCB.CheckedChanged += new System.EventHandler(this.MinimizeToSysTrayCB_CheckedChanged);
            // 
            // CloseMinimizesCB
            // 
            this.CloseMinimizesCB.AutoSize = true;
            this.CloseMinimizesCB.Location = new System.Drawing.Point(35, 25);
            this.CloseMinimizesCB.Name = "CloseMinimizesCB";
            this.CloseMinimizesCB.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.CloseMinimizesCB.Size = new System.Drawing.Size(109, 17);
            this.CloseMinimizesCB.TabIndex = 6;
            this.CloseMinimizesCB.Text = "Minimize on close";
            this.CloseMinimizesCB.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.CloseMinimizesCB.UseMnemonic = false;
            this.CloseMinimizesCB.UseVisualStyleBackColor = true;
            this.CloseMinimizesCB.CheckedChanged += new System.EventHandler(this.CloseToSysTrayCB_CheckedChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(3, 25);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 13);
            this.label3.TabIndex = 3;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.CachePathTB);
            this.groupBox3.Controls.Add(this.CachePathL);
            this.groupBox3.Location = new System.Drawing.Point(12, 182);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(410, 65);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Advanced settings";
            // 
            // ResetSettingsButton
            // 
            this.ResetSettingsButton.Location = new System.Drawing.Point(299, 253);
            this.ResetSettingsButton.Name = "ResetSettingsButton";
            this.ResetSettingsButton.Size = new System.Drawing.Size(122, 23);
            this.ResetSettingsButton.TabIndex = 9;
            this.ResetSettingsButton.Text = "Reset default settings";
            this.ResetSettingsButton.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.ResetSettingsButton.UseVisualStyleBackColor = true;
            this.ResetSettingsButton.Click += new System.EventHandler(this.ResetSettingsButton_Click);
            // 
            // CancelButton
            // 
            this.CancelButton.Location = new System.Drawing.Point(11, 282);
            this.CancelButton.Name = "CancelButton";
            this.CancelButton.Size = new System.Drawing.Size(75, 23);
            this.CancelButton.TabIndex = 10;
            this.CancelButton.Text = "Cancel";
            this.CancelButton.UseVisualStyleBackColor = true;
            this.CancelButton.Click += new System.EventHandler(this.CancelButton_Click);
            // 
            // ApplyButton
            // 
            this.ApplyButton.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ApplyButton.Location = new System.Drawing.Point(265, 282);
            this.ApplyButton.Name = "ApplyButton";
            this.ApplyButton.Size = new System.Drawing.Size(75, 23);
            this.ApplyButton.TabIndex = 11;
            this.ApplyButton.Text = "Apply";
            this.ApplyButton.UseVisualStyleBackColor = false;
            this.ApplyButton.Click += new System.EventHandler(this.ApplyButton_Click);
            // 
            // OkButton
            // 
            this.OkButton.BackColor = System.Drawing.SystemColors.ControlLight;
            this.OkButton.Location = new System.Drawing.Point(346, 282);
            this.OkButton.Name = "OkButton";
            this.OkButton.Size = new System.Drawing.Size(75, 23);
            this.OkButton.TabIndex = 0;
            this.OkButton.Text = "Ok";
            this.OkButton.UseVisualStyleBackColor = false;
            this.OkButton.Click += new System.EventHandler(this.OkButton_Click);
            // 
            // SettingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(433, 317);
            this.Controls.Add(this.OkButton);
            this.Controls.Add(this.ApplyButton);
            this.Controls.Add(this.CancelButton);
            this.Controls.Add(this.ResetSettingsButton);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "SettingsForm";
            this.Text = "Settings";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TextBox CachePathTB;
        private System.Windows.Forms.Label CachePathL;
        private System.Windows.Forms.Label DownloadPathL;
        private System.Windows.Forms.TextBox DownloadPathTB;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.CheckBox EnableNotiCB;
        private System.Windows.Forms.CheckBox MinimizeToSysTrayCB;
        private System.Windows.Forms.CheckBox CloseMinimizesCB;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button ResetSettingsButton;
        private System.Windows.Forms.Button CancelButton;
        private System.Windows.Forms.Button ApplyButton;
        private System.Windows.Forms.Button OkButton;
        private System.Windows.Forms.CheckBox RemoveFooterCB;
        private System.Windows.Forms.CheckBox StartMinimizedCB;
    }
}