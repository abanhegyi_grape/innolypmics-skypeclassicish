﻿using System;
using System.IO;
using System.Windows.Forms;
using Syroot.Windows.IO;
using SkypeClassicish.Helpers;

namespace SkypeClassicish
{
    public partial class SettingsForm : Form
    {
        private readonly ToolTip toolTip = new ToolTip();
        private long AnyChangedFlag = 0L;


        public SettingsForm()
        {
            InitializeComponent();

            ApplyButton.Enabled = false;
            ((Form) this).CancelButton = CancelButton;

            SetTooltip();

            int tempRef = 0;
            SetPositionNumberForControls(ref tempRef, this.Controls);

            SetValues();
        }

        private void SetValues()
        {
            SetValues(Properties.Settings.Default);
        }

        private void ResetToDefault()
        {
            var resetSettings = new Properties.Settings();
            resetSettings.Reset();
            SetValues(resetSettings);

            Properties.Settings.Default.Save(); //just to be safe...
        }

        private void SetValues(Properties.Settings settings)
        {
            CachePathTB.Text = settings.CachePath;
            DownloadPathTB.Text = settings.DownloadPath;
            CloseMinimizesCB.Checked = settings.CloseMinimizes;
            EnableNotiCB.Checked = settings.EnableNotifications;
            MinimizeToSysTrayCB.Checked = settings.MinimizeToSystemTray;
            RemoveFooterCB.Checked = !settings.ShowFooter;
            StartMinimizedCB.Checked = settings.StartMinimized;
        }

        private void SetPositionNumberForControls(ref int position, Control.ControlCollection controls)
        {
            foreach (Control control in controls)
            {
                if (control is TextBox || control is CheckBox)
                {
                    control.Tag = position++;
                }
                else if (control is GroupBox)
                {
                    SetPositionNumberForControls(ref position, (control as GroupBox).Controls);
                }
            }
        }

        private void SetTooltip()
        {
            toolTip.AutoPopDelay = short.MaxValue;   //internally, it uses signed 16bit integer (aka short), so this is ~33sec delay, the max
            toolTip.UseAnimation = true;
            toolTip.UseFading = true;

            toolTip.SetToolTip(this.DownloadPathTB, "The path where files and images will be downloaded.\nLeave empty to be asked each time about the location.");
            toolTip.SetToolTip(this.DownloadPathL, toolTip.GetToolTip(this.DownloadPathTB));
            toolTip.SetToolTip(this.CachePathTB, "The path where the application will save it's cache.\nIf empty, it will be where the application is located.\nNeeds restarting to apply and the old cache will not be deleted or moved.");
            toolTip.SetToolTip(this.CachePathL, toolTip.GetToolTip(this.CachePathTB));

            toolTip.SetToolTip(this.CloseMinimizesCB, "If set, the application will minimize instead of closing.\nYou can still close it through the System Tray icon or through Task Manager.");
            toolTip.SetToolTip(this.MinimizeToSysTrayCB, "If set, the application will hide to System Tray instead of minimizing.");
            toolTip.SetToolTip(this.EnableNotiCB, "If set, the application will show desktop notifications.");
            toolTip.SetToolTip(this.StartMinimizedCB, "If set, the application will start minized.\nUseful if you make the application run at startup.");
            toolTip.SetToolTip(this.RemoveFooterCB, "If set, the footer inside Skype will be removed.");
        }

        private void CancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ApplyButton_Click(object sender, EventArgs e)
        {
            Properties.Settings.Default.CachePath = CachePathTB.Text;
            Properties.Settings.Default.CloseMinimizes = CloseMinimizesCB.Checked;
            Properties.Settings.Default.EnableNotifications = EnableNotiCB.Checked;
            Properties.Settings.Default.MinimizeToSystemTray = MinimizeToSysTrayCB.Checked;
            Properties.Settings.Default.DownloadPath = DownloadPathTB.Text;
            Properties.Settings.Default.ShowFooter = !RemoveFooterCB.Checked;
            Properties.Settings.Default.StartMinimized = StartMinimizedCB.Checked;

            Properties.Settings.Default.Save();

            ApplyButton.Enabled = false;
        }

        private void OkButton_Click(object sender, EventArgs e)
        {
            ApplyButton_Click(sender, e);
            CancelButton_Click(sender, e);
        }

        private void DownloadPathTB_Enter(object sender, EventArgs e)
        {
            NativesHelper.HideCaretInTextBox(DownloadPathTB);
            DownloadPathTB.Text = SelectFolder(DownloadPathTB.Text, KnownFolders.Downloads.Path);
            NativesHelper.HideCaretInTextBox(DownloadPathTB);
        }

        private void CachePathTB_Enter(object sender, EventArgs e)
        {
            NativesHelper.HideCaretInTextBox(CachePathTB);
            CachePathTB.Text = SelectFolder(CachePathTB.Text, Path.GetDirectoryName(Application.ExecutablePath));
            NativesHelper.HideCaretInTextBox(CachePathTB);
        }

        private string SelectFolder(string currentFolder, string defaultStartFolder)
        {
            var dial = new FolderBrowserDialog
            {
                ShowNewFolderButton = true
            };

            if (currentFolder == string.Empty && Directory.Exists(defaultStartFolder))
            {
                dial.SelectedPath = defaultStartFolder;
            }
            else
            {
                dial.SelectedPath = currentFolder;
            }

            if (dial.ShowDialog() == DialogResult.OK)
            {
                return dial.SelectedPath + '\\';
            }

            return currentFolder;
        }

        private void DownloadPathTB_TextChanged(object sender, EventArgs e)
        {
            if (DownloadPathTB.Text != Properties.Settings.Default.DownloadPath)
            {
                AnyChangedFlag.AddFlag((int) DownloadPathTB.Tag);
            }
            else
            {
                AnyChangedFlag.RemoveFlag((int)DownloadPathTB.Tag);
            }

            ApplyButton.Enabled = AnyChangedFlag.HasAnySet();
        }

        private void CloseToSysTrayCB_CheckedChanged(object sender, EventArgs e)
        {
            if (CloseMinimizesCB.Checked != Properties.Settings.Default.CloseMinimizes)
            {
                AnyChangedFlag.AddFlag((int) CloseMinimizesCB.Tag);
            }
            else
            {
                AnyChangedFlag.RemoveFlag((int) CloseMinimizesCB.Tag);
            }

            ApplyButton.Enabled = AnyChangedFlag.HasAnySet();
        }

        private void MinimizeToSysTrayCB_CheckedChanged(object sender, EventArgs e)
        {
            if (MinimizeToSysTrayCB.Checked != Properties.Settings.Default.MinimizeToSystemTray)
            {
                AnyChangedFlag.AddFlag((int) MinimizeToSysTrayCB.Tag);
            }
            else
            {
                AnyChangedFlag.RemoveFlag((int) MinimizeToSysTrayCB.Tag);
            }

            ApplyButton.Enabled = AnyChangedFlag.HasAnySet();
        }

        private void EnableNotiCB_CheckedChanged(object sender, EventArgs e)
        {
            if (EnableNotiCB.Checked != Properties.Settings.Default.EnableNotifications)
            {
                AnyChangedFlag.AddFlag((int) EnableNotiCB.Tag);
            }
            else
            {
                AnyChangedFlag.RemoveFlag((int) EnableNotiCB.Tag);
            }

            ApplyButton.Enabled = AnyChangedFlag.HasAnySet();
        }

        private void StartMinimizedCB_CheckedChanged(object sender, EventArgs e)
        {
            if (StartMinimizedCB.Checked != Properties.Settings.Default.StartMinimized)
            {
                AnyChangedFlag.AddFlag((int)StartMinimizedCB.Tag);
            }
            else
            {
                AnyChangedFlag.RemoveFlag((int)StartMinimizedCB.Tag);
            }

            ApplyButton.Enabled = AnyChangedFlag.HasAnySet();
        }

        private void CachePathTB_TextChanged(object sender, EventArgs e)
        {
            if (CachePathTB.Text != Properties.Settings.Default.CachePath)
            {
                AnyChangedFlag.AddFlag((int) CachePathTB.Tag);
            }
            else
            {
                AnyChangedFlag.RemoveFlag((int) CachePathTB.Tag);
            }

            ApplyButton.Enabled = AnyChangedFlag.HasAnySet();
        }

        private void RemoveFooterCB_CheckedChanged(object sender, EventArgs e)
        {
            if (RemoveFooterCB.Checked != !Properties.Settings.Default.ShowFooter)
            {
                AnyChangedFlag.AddFlag((int) RemoveFooterCB.Tag);
            }
            else
            {
                AnyChangedFlag.RemoveFlag((int) RemoveFooterCB.Tag);
            }

            ApplyButton.Enabled = AnyChangedFlag.HasAnySet();
        }

        private void ResetSettingsButton_Click(object sender, EventArgs e)
        {
            ResetToDefault();
        }
    }
}
