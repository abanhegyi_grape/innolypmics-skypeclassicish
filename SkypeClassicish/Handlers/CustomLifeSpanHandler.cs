﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CefSharp;
using SkypeClassicish.Helpers;

namespace SkypeClassicish.Handlers
{
    class CustomLifeSpanHandler : ILifeSpanHandler
    {
        public bool OnBeforePopup(IWebBrowser chromiumWebBrowser, IBrowser browser, IFrame frame, string targetUrl, string targetFrameName, WindowOpenDisposition targetDisposition, bool userGesture, IPopupFeatures popupFeatures, IWindowInfo windowInfo, IBrowserSettings browserSettings, ref bool noJavascriptAccess, out IWebBrowser newBrowser)
        {
            if (targetUrl.Contains("api.asm.skype.com/v1/objects") && targetUrl.Contains("imgpsh"))
            {
                chromiumWebBrowser.Download(targetUrl);
            }

            BrowserHelpers.OpenInDefaultBrowser(targetUrl);

            newBrowser = null;
            return true;
        }

        public void OnAfterCreated(IWebBrowser chromiumWebBrowser, IBrowser browser)
        { }

        public bool DoClose(IWebBrowser chromiumWebBrowser, IBrowser browser)
        {
            browser.CloseBrowser(true);
            browser.Dispose();
            return false;
        }

        public void OnBeforeClose(IWebBrowser chromiumWebBrowser, IBrowser browser)
        { }
    }
}
