﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CefSharp;
using SkypeClassicish.Helpers;

namespace SkypeClassicish.Handlers
{
    class CustomDownloadHandler : IDownloadHandler
    {
        public event EventHandler<DownloadItem> OnBeforeDownloadEvent;
        public event EventHandler<DownloadItem> OnDownloadUpdatedEvent;

        public void OnBeforeDownload(IWebBrowser chromiumWebBrowser, IBrowser browser, DownloadItem downloadItem, IBeforeDownloadCallback callback)
        {
            try
            {
                //images are handled from JavaScript
                if (!downloadItem.OriginalUrl.Contains("imgpsh"))
                {
                    chromiumWebBrowser.ExecuteScriptAsync("HighlightFile", downloadItem.SuggestedFileName);
                }

                OnBeforeDownloadEvent?.Invoke(this, downloadItem);

                string filePath = Properties.Settings.Default.DownloadPath + downloadItem.SuggestedFileName;

                if (File.Exists(filePath)) //hopefully filenames are unique!
                {
                    //if it exists, open it instead of downloading
                    BrowserHelpers.TryOpen(filePath);
                }
                else if (!callback.IsDisposed)
                {
                    using (callback)
                    {
                        callback.Continue(filePath, Properties.Settings.Default.DownloadPath == string.Empty);
                    }
                }
            }
            catch(Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
            }
        }

        public void OnDownloadUpdated(IWebBrowser chromiumWebBrowser, IBrowser browser, DownloadItem downloadItem, IDownloadItemCallback callback)
        {
            OnDownloadUpdatedEvent?.Invoke(this, downloadItem);

            //CEF is too lazy to set SuggestedFileName for files, so improvisation ensues
            var fileName = downloadItem.ContentDisposition.Split('=').Last().Trim('"');

            try
            {
                //images are handled from JavaScript where I actually have their URLs
                if (downloadItem.IsCancelled)
                {
                    if (!downloadItem.OriginalUrl.Contains("imgpsh"))
                    {
                        chromiumWebBrowser.ExecuteScriptAsync("AddBorderToFile", fileName, "red");
                    }
                }
                else if (downloadItem.IsInProgress)
                {
                    if (!downloadItem.OriginalUrl.Contains("imgpsh"))
                    {
                        chromiumWebBrowser.ExecuteScriptAsync("AddBorderToFile", fileName, "blue");
                    }
                }
                else if (downloadItem.IsComplete)
                {
                    if (!downloadItem.OriginalUrl.Contains("imgpsh"))
                    {
                        chromiumWebBrowser.ExecuteScriptAsync("AddBorderToFile", fileName, "green");
                    }
                }
            }
            catch(Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
            }
        }
    }
}
