﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CefSharp;
using CefSharp.WinForms.Internals;

namespace SkypeClassicish.Handlers
{
    class CustomFocusHandler : DefaultFocusHandler
    {
        public override bool OnSetFocus(IWebBrowser chromiumWebBrowser, IBrowser browser, CefFocusSource source)
        {
            if (browser == null)
            {
                return true;
            }

            return base.OnSetFocus(chromiumWebBrowser, browser, source);
        }
    }
}
