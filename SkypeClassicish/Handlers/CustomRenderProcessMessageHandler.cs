﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CefSharp;
using CefSharp.WinForms;

namespace SkypeClassicish.Handlers
{
    public class CustomRenderProcessMessageHandler : IRenderProcessMessageHandler
    {
        public event Action OnContextCreatedEvent;

        public void OnContextCreated(IWebBrowser browserControl, IBrowser browser, IFrame frame)
        {
            OnContextCreatedEvent?.Invoke();
        }

        public void OnContextReleased(IWebBrowser browserControl, IBrowser browser, IFrame frame)
        { }

        public void OnFocusedNodeChanged(IWebBrowser browserControl, IBrowser browser, IFrame frame, IDomNode node)
        { }

        public void OnUncaughtException(IWebBrowser browserControl, IBrowser browser, IFrame frame, JavascriptException exception)
        { }
    }
}
