using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using CefSharp;
using CefSharp.Handler;
using SkypeClassicish.Helpers;

namespace SkypeClassicish.Handlers
{
    internal class CustomRequestHandler : DefaultRequestHandler
    {
        public override bool OnOpenUrlFromTab(IWebBrowser browserControl, IBrowser browser, IFrame frame, string targetUrl, WindowOpenDisposition targetDisposition, bool userGesture)
        {
            if (targetUrl.Contains("api.asm.skype.com/v1/objects") && targetUrl.Contains("imgpsh")) 
            {
                browserControl.Download(targetUrl);
            }

            BrowserHelpers.OpenInDefaultBrowser(targetUrl);

            return true;
        }

        public override bool OnBeforeBrowse(IWebBrowser browserControl, IBrowser browser, IFrame frame, IRequest request, bool userGesture, bool isRedirect)
        {
            if (request.Url.Contains("api.asm.skype.com/v1/objects") && request.Url.Contains("imgpsh"))
            {
                browserControl.Download(request.Url);
                return true;
            }

            if (request.Url.Contains("skype") || request.ReferrerUrl.Contains("login") || request.Url.Contains("c1.microsoft.com") || request.Url.Contains("fpt2.microsoft.com"))
            {
                return base.OnBeforeBrowse(browserControl, browser, frame, request, userGesture, isRedirect);
            }

            BrowserHelpers.OpenInDefaultBrowser(request.Url);

            return true;
        }
    }
}
