Notification.requestPermission = function ()
{
    return "granted";
};

function Notification(title, options)
{
    //most of these are just to prevent JS errors, not that they matter...
    //my implementation of notifications isn't robust enough to handle even half of these :)
    this.title = title;
    this.dir = !options.dir ? "auto" : options.dir;
    this.lang = !options.lang ? "" : options.lang;
    this.badge = !options.badge ? "" : options.badge;
    this.body = !options.body ? "" : options.body;
    this.tag = !options.tag ? "" : options.tag;
    this.icon = !options.icon ? "" : options.icon;
    this.image = !options.image ? "" : options.image;
    this.data = !options.data ? "" : options.data;
    this.vibrate = !options.vibrate ? "" : options.vibrate;
    this.renotify = !options.renotify ? false : options.renotify;
    this.requireInteraction = !options.requireInteraction ? false : options.requireInteraction;
    this.actions = !options.actions ? "" : options.actions;
    this.silent = !options.silent ? false : options.silent;
    this.sound = !options.sound ? "" : options.sound;
    this.noscreen = !options.noscreen ? false : options.noscreen;
    this.sticky = !options.sticky ? false : options.sticky;

    this.close = function()
    { /*can't properly handle this*/ };

    //these are meant to be set by skype
    this.onclick = function() { };
    this.onerror = function() { };
    this.onclose = function () { };
    this.onshow = function () { };

    /*
    //using these for my custom handler to ensure that the most up-to-date version of these functions are called
    this.callOnClick = async function() { this.onclick(); };
    this.callOnError = async function() { this.onerror(); };    //inside the functions, the 'this' was pointing at window... because reasons
    this.callOnClose = async function() { this.onclose(); };    //sooo I had to take the looong way around, see below...
    this.callOnShow = async function() { this.onshow(); };

    JavaScriptNotificationHandler.fireNotification(this.title, this.body, this.icon, this.callOnClick, this.callOnShow, this.callOnError, this.callOnClose);
    */

    var notif = this;
    var callOnClick = function () { notif.onclick(); };
    var callOnShow = function () { notif.onshow(); };
    var callOnError = function () { notif.onerror(); };
    var callOnClose = function () { notif.onclose(); };

    JavaScriptNotificationHandler.fireNotification(this.title, this.body, this.icon, callOnClick, callOnShow, callOnError, callOnClose);
}

Notification.permission = "granted";