﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SkypeClassicish.Helpers
{
    public static class ExtensionMethods
    {
        private static readonly string[] blackList;

        static ExtensionMethods()
        {
            blackList = new string[]
            {
                "bn2.fpt.microsoft.com",            //no idea why but this sometimes pops up while it shouldn't
                "api.asm.skype.com/v1/objects",     //files and images, we want to download these, not open in browser (although this should never show up)
                "login.live.com",                   //login site, we don't want to go there
                "about:blank"                       //for obvious reasons
            };
        }

        /*
         * 1. shift so that the bit marked by 'position' is the last
         * 2. mask the result so that every other bit is 0
         * 3. see if the result is 1
         * 4. if it is, the bit is set, return true, else false
         */
        public static bool HasFlag(this long flagVar, int position) => ((flagVar >> position) & 1L) == 1L;

        /*
         * 1. shift 1 into the bit position marked by 'position'
         * 2. set that single bit to true in 'flagVar' with bitwise OR
         */
        public static void AddFlag(ref this long flagVar, int position) => flagVar |= 1L << position;

        /*
         * 1. shift 1 into the bit position marked by 'position'
         * 2. negate that number so that every other bit is 1, except that one
         * 3. set that single bit to false with bitwise AND
         */
        public static void RemoveFlag(ref this long flagVar, int position) => flagVar &= ~(1L << position);

        public static bool HasAnySet(this long flagVar) => flagVar != 0;


        public static bool IsBlackListed(this string url)
        {
            foreach (var item in blackList)
            {
                if (url.Contains(item))
                {
                    Debug.WriteLine($"Blacklisted: {url}");
                    return true;
                }
            }

            return false;
        }
    }
}
