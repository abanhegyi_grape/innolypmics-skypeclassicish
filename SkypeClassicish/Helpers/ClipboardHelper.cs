﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SkypeClassicish.Helpers
{
    public static class ClipboardHelper
    {
        public static bool ContainsImage()
        {
            bool result = false;

            RunInSTA(delegate () {
                result = Clipboard.ContainsImage();
            });

            return result;
        }
        public static bool ContainsFileDropList()
        {
            bool result = false;

            RunInSTA(delegate () {
                result = Clipboard.ContainsFileDropList();
            });

            return result;
        }
        public static System.Collections.Specialized.StringCollection GetFileDropList()
        {
            System.Collections.Specialized.StringCollection result = null;

            RunInSTA(delegate () {
                result = Clipboard.GetFileDropList();
            });

            return result;
        }
        public static Image GetImage()
        {
            Image result = null;

            RunInSTA(delegate () {
                result = Clipboard.GetImage();
            });

            return result;
        }

        private static void RunInSTA(ThreadStart deleg)
        {
            var staThread = new Thread(deleg);
            staThread.SetApartmentState(ApartmentState.STA);
            staThread.Start();
            staThread.Join();
        }
    }
}
