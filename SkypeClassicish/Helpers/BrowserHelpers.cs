﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CefSharp;

namespace SkypeClassicish.Helpers
{
    public static class BrowserHelpers
    {
        public static void Download(this IWebBrowser browser, string url)
        {
            try
            {
                browser.ExecuteScriptAsync("DownloadFromUrl", url);
            }
            catch(Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
            }
        }

        public static void OpenInDefaultBrowser(string url)
        {
            if (!url.IsBlackListed())
            {
                TryOpen(url);
            }
        }

        public static bool TryOpen(string path)
        {
            try
            {
                System.Diagnostics.Process.Start(path);
            }
            catch(Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                return false;
            }

            return true;
        }
    }
}
