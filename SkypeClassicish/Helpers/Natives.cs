﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace SkypeClassicish.Helpers
{
    internal class Natives
    {
        [DllImport("user32")]
        internal static extern bool PostMessage(IntPtr hwnd, int msg, IntPtr wparam, IntPtr lparam);

        [DllImport("user32")]
        internal static extern int RegisterWindowMessage(string message);

        [DllImport("user32")]
        internal static extern bool HideCaret(IntPtr hwnd);

        [DllImport("user32.dll")]
        internal static extern int SetForegroundWindow(IntPtr hwnd);
    }
}
