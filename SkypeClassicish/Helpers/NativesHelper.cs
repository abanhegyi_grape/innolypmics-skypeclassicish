﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SkypeClassicish.Helpers
{
    static class NativesHelper
    {
        private static readonly IntPtr HWND_BROADCAST = new IntPtr(0xffff);  //broadcast the message

        public static int FocusMessageBroadcastMessageId { get; } = Natives.RegisterWindowMessage(Properties.Settings.Default.FocusMessageBroadcast);

        public static void BroadcastFocusMessage() => Natives.PostMessage(HWND_BROADCAST, FocusMessageBroadcastMessageId, IntPtr.Zero, IntPtr.Zero);

        public static void HideCaretInTextBox(TextBox textBox) => Natives.HideCaret(textBox.Handle);

        public static void SetFocusToForm(this Form form) => Natives.SetForegroundWindow(form.Handle);
    }
}
