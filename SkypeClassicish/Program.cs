﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using SkypeClassicish.Helpers;

namespace SkypeClassicish
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            System.Threading.Mutex mutex = new System.Threading.Mutex(true, Properties.Settings.Default.MutexName);

            if (mutex.WaitOne(0))
            {   //we are the first 
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new MainForm());
                mutex.ReleaseMutex();
            }
            else
            {
                NativesHelper.BroadcastFocusMessage();
            }
        }
    }
}
