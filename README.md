This is a project aimed at creating a Skype Classic-like user experience using
Skype for Web and CefSharp written in C# with (for a .NET Winforms application)
heavy use of Javascript.

Dependencies to run: 
- Visual C++ Redistributable 2015
- .NET Framework 4.7.2
- Windows 7+

Dependencies to build:
- Visual Studio 2013+
- Windows Universal CRT SDK for Visual Studio
- Dependencies to run

Installation:
- Make sure you have all the dependencies needed
- Either build the project or extract the SkypeClassicish_beta.rar file
- Run SkypeClassicish.exe
- That's it, the app is portable

Build Guide:
- Make sure you have all the dependencies installed
- Make sure you have internet connection for the NuGet packages
- Open the SkypeClassicish.sln file in the CefSharp3 folder or the shortcut
  pointing to it in the main folder of the solution
- Set build target to Release or Debug in Win32
- Set SkypeClassicish as StartUp Project or select build from the
  context menu

User Guide:
- SkypeClassicish is a browser, no matter what you might think, so you NEED
  internet connection to use the application. Else you'll be greeted with a
  blue screen unchanging until Chromium times out with the request and
  fires the LoadError event. After that, the application will try and reload
  the page but this can happen, however unfortunate it may be.
  However, if you are greeted with a gray background, the browser crashed.
  If reproducable, contact me and I'll try and fix it.
- The default display is the normal window opening up, shortly after the
  webpage loading in where you need to login. You can save your login details
  AFTER you entered your email address/username but before you entered your
  password. This is how Skype for Web works. Then, you are greeted with the
  familiar(ish) Skype look. You will notice if you have used Skype for Web
  that there are no prompts to welcome you to Skype for Web (aka 'annoy you
  every 2nd time you login') or to download Skype for Desktop.
  These have been removed dynamically. You are welcome.
  NOTE: The site is still saving cookies, it just won't notify you. It can't
        anymore. However, they will only ever be used through SkypeClassicish.
- Microphone and Webcam access are enabled by default. Currently there are no
  options inside the app to disable this.
- No screenshare functionality. This is more of a limitation of Skype for Web
  than the app itself, but there is a way to solve it. It would just need way
  too much work for now.
- Image and file pasting. File pasting is not a feature in Skype for Web btw.
- Save and open files. You can only open files from SkypeClassicish if you have
  a download location set. This is because the application doesn't save where
  you have saved the files and as such can't open them. This is a limitation
  of the implementation of file saving on the part of CEF, which can be
  circumwented, though not easily. However, if notifications are enabled, you
  can open them by clicking the notification. Either way, I managed to
  implement a somewhat acceptable way to notify the user about the success
  or failure of downloads (Skype for Web relies on the browser completely
  for that...), so at least you'll know whether the file has been downloaded
  or not even without notifications (until Skype garbage collects the
  undisplayed DOM elements at which point my changes to the elements will be
  lost. Just don't be surprised if, after a while, a file doesn't show
  downloaded inside Skype after having successfully downloaded it.
- Skype has desktop notifications, so do SkypeClassicish. They are enabled by
  default and work as expected. These are the default Windows notifications,
  so they are not as pretty, but they do the job. Anyone welcome to make custom
  notifications and make a pull request.
- System Tray icon with Settings through the context menu.
- Only one SkypeClassicish instance. This is to make hiding work as expected.
- Otherwise, all features of Skype for Web are available, including a dark
  theme. If it turns out one or more are not, request those features and I'll
  try and implement them as soon as I can.

Settings:
- The settings are available from the System Tray icon in the context menu.
  Every option mentioned below is also explained through tooltips there.
- Download Path. Set where you want to download files downloaded from Skype.
  If not set, every time you try and download a file, you will be prompted
  for a location. If you enable it, after successfully downloading a file,
  you will be able to open it by clicking the download icon in SkypeClassicish.
- Minimize on close. If enabled, you won't accidentally close the application
  when you just wanted to get rid of clutter on your screen. Disabled by
  default.
- Minimize to System Tray. If enabled, when the application would minimize,
  it will instead hide to the System Tray.
- Remove footer. If you want to remove the footer from inside Skype, you can.
  Disabled by default.
- Enable Notifications. If set, the application will show desktop notifications.
  Enabled by default.
- Remove footer. If enabled, Skype won't display it's useless footer. Disabled
  by default, as it does have some functionality that can be useful the first
  time the application runs, like settings language.
- Start minimized. If enabled, the application will start minimized. Preferable
  when you make the application run at startup. It might still show up for a
  split second though on slower computers.
- Cache path. This is not set by default. The functionality when it's not set
  is that the application will save it's Cache to the application's folder.
  It needs restarting to apply.
- There is also a button to reset the settings to their default state.
  You still need to apply the settings for it to save.