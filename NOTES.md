These are just some notes ordered randomly. Good-to-know stuff, if you like.

1.  The forms need refactoring badly. Especially the Main form, it's
    a cluttered mess. Alas, no time for a good quality refactoring,
    as it would likely take several hours.
2.  If you see a global object in Javascript that you don't know, it's
    likely an object exposed through CefSharp. These objects are:
    - CefSharp (exposed by default)
    - JavaScriptClipboardFileHandler (found in BoundJsObjects)
    - JavaScriptNotificationHandler (found in BoundJsObjects)
    - JavaScriptMimeTypeHelper (found in BoundJsObjects)
    They are exposed dynamically in BindJsObjects inside the Main form.
3.  The file inject.js is injected in CefSharp.BrowserSubprocess.Core
    in CefAppUnmanagedWrapper.cpp, look for GRAPE and you'll find the code.
    This was necessary as I needed to have the Notification class overwritten
    as soon as possible, and triggering from C# in an async fashion (the only
    way from C#) had a chance to be too late. So I went native with it instead.
4.  The Javascript files could be optimized to run better in many cases.
    This wasn't a priority at all, but could be done.
    However, the performance impact of my code is lot less than what
    Skype is already running, however, it still could be noticable in
    lower-end systems.
5.  The inject.js is responsible for notifications and notifications only.
6.  The skypeClassicish.js file is responsible for file and image pasting,
    downloading images from the web instead of opening them (Cef handles files
    natively, it's just that it tries to open links pointing to images like
    any normal webpage), hiding or showing the footer, removing useless or even
    annoying stuff like "Welcome to Skype for Web, Get Started", and fixing
    pages trying to open in popups but CEF not exposing the actual href.
    This file could really be optimized, especially the MutationObservers
    and the link fixing part. Also, the recursive GetAncestor.
7.  Mutexes play a small but really important role in this application.
    They are used to implement singleton application logic and also
    to make sure that no BrowserSubprocess remains openned after closing
    the actual application.
    Hint:   If you need multiple SkypeClassicish instances running,
            just edit the Settings file between building. You need to
            change the MutexName and FocusMessageBroadcast to something
            else and that will decouple the different builds completely.
8.  The logic to write notification numbers onto an Icon object
    [Icon DrawNotificationOnIcon(Icon, int)] is quite modular internally.
    If you want other colors, other size, other position, etc, you can
    just change he maths behind those single parameters and it should come
    out OK if not perfect. Just don't mess with the actual drawing logic
    other than the color and maybe shape and you can do some pretty stuff.
9.  The Icon file used inside the application and the .NET Paint project file
    used to create it are both included. They are self-made and feel free to
    change them however you like. Just make sure you remove the lower
    resolution internal icons from the icon file as Windows really wants to
    just use the 32x32 version and will do so if you give it a chance...
10. Since updating to CefSharp 67.0 stable, sometimes CEF just hangs at cloing.
    To fix this, I am using a simple timeout Task with Environment.Exit.
    It works but it's not pretty. Luckily, thanks to the mutexes, every
    unreleased resource (read: the processes) are closed. 
11. If I had (much more) time I would have implemented two more things.
    The first is a Notification form instead of the Windows notifications.
    They are just not as eye-catching and lack customizability.
    Also, maybe possibility to have sound for notifications, but that's
    not important (to me, it is for others...).
    They would make the app just that much more user friendly.
    The other feature is screen sharing (read 12.).
12. Even though Skype for Web just doesn't support screen sharing, it COULD
    be implemented through some heavy trickery. The idea would be this:
    - capture the screen from C#
    - send the data to a driver
    - have the driver pose as a webcam to Windows
    These three steps would enable the user to use video calls as a way
    to share their screen. Would it be pretty? No. Would it work. It should.
    And the ONLY limitation this would impose is that they couldn't share their
    screen AND also show their real webcam's footage. I could live with that.
    Sadly, this would take weeks to implement at least, especially as I quite
    literally have no knowledge how to write a driver, let alone hook it up
    with C# code and activate screen capture when needed.
13. Under (very specific) circumstances, the application might crash and this
    is practically unpreventable without putting everything in try-catch
    blocks or specific checks everywhere where you need to call something CEF
    specific. The cause of this is that when you close the application, some
    threads started from CEF that call back into C# code execute that C# code
    after CEF has been shut down. I tried to wrap everything in try-catch
    but as these are not WinForms UI threads, there is no 'catch-all' solution.